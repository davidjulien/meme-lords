#ifndef _FILE_H_
#define _FILE_H_

#include <limits.h>
#include <proc.h>

#define END_OF_FILE 9223372036854775807

struct file
{
	char p_name[NAME_MAX];
	struct vnode *p_vn;
	int flags;
	mode_t mode;
	off_t offset;
	int reference_count;
} file;

struct file_descriptor
{
	struct file *file;
	struct lock *file_lock;
};

/* File Descriptors */
int fds_init(struct proc *process);

int is_fd_out_of_bounds(int fd);

#endif