#include <kern/unistd.h>
#include <types.h>
#include <syscall.h>
#include <vnode.h>
#include <vfs.h>
#include <kern/fcntl.h>
#include <kern/errno.h>
#include <current.h>
#include <proc.h>
#include <file.h>
#include <synch.h>


int
is_fd_out_of_bounds(int fd)
{
	return 
		fd < 0 ||
		fd >= OPEN_MAX;
}

int
fds_init(struct proc *process)
{
	int error = 0;

	// Vnodes
	struct vnode *vn_stdin = NULL;
	struct vnode *vn_stdout = NULL;
	struct vnode *vn_stderr = NULL;

	// Filenames
	char *stdin = kstrdup("con:");
	char *stdout = kstrdup("con:");
	char *stderr = kstrdup("con:");

	if ((error = vfs_open(stdin, O_RDONLY, 0, &vn_stdin)))
	{
		kfree(stdin);
		return error;
	}

	if ((error = vfs_open(stdout, O_WRONLY, 0, &vn_stdout)))
	{
		kfree(stdin);
		kfree(stdout);
		vfs_close(vn_stdin);
		return error;
	}

	if ((error = vfs_open(stderr, O_RDWR, 0, &vn_stderr)))
	{
		kfree(stdin);
		kfree(stdout);
		kfree(stderr);
		vfs_close(vn_stdin);
		vfs_close(vn_stdout);
		return error;
	}

	for (int i = 0; i < OPEN_MAX; i++)
	{
		process->pp_fd[i] = (struct file_descriptor *)kmalloc(sizeof(struct file_descriptor));
		process->pp_fd[i]->file = NULL;
		process->pp_fd[i]->file_lock = lock_create("file lock");
	}

	// stdin
	process->pp_fd[0]->file = (struct file *)kmalloc(sizeof(struct file));

	if (!process->pp_fd[0]->file)
	{
		kfree(stdin);
		kfree(stdout);
		kfree(stderr);
		vfs_close(vn_stdin);
		vfs_close(vn_stdout);
		vfs_close(vn_stderr);
		return ENOMEM;
	}

	strcpy(process->pp_fd[0]->file->p_name, stdin);
	process->pp_fd[0]->file->p_vn = vn_stdin;
	process->pp_fd[0]->file->mode = O_RDONLY;
	process->pp_fd[0]->file->flags = O_RDONLY;
	process->pp_fd[0]->file->offset = 0;
	process->pp_fd[0]->file->reference_count = 1;

	// stdout
	process->pp_fd[1]->file = (struct file *)kmalloc(sizeof(struct file));

	if (!process->pp_fd[1]->file)
	{
		kfree(stdin);
		kfree(stdout);
		kfree(stderr);
		vfs_close(vn_stdin);
		vfs_close(vn_stdout);
		vfs_close(vn_stderr);
		kfree(process->pp_fd[0]->file);
		return ENOMEM;
	}

	strcpy(process->pp_fd[1]->file->p_name, stdout);
	process->pp_fd[1]->file->p_vn = vn_stdout;
	process->pp_fd[1]->file->mode = O_WRONLY;
	process->pp_fd[1]->file->flags = O_WRONLY;
	process->pp_fd[1]->file->offset = 0;
	process->pp_fd[1]->file->reference_count = 1;

	// stdout
	process->pp_fd[2]->file = (struct file *)kmalloc(sizeof(struct file));

	if (!process->pp_fd[2]->file)
	{
		kfree(stdin);
		kfree(stdout);
		kfree(stderr);
		vfs_close(vn_stdin);
		vfs_close(vn_stdout);
		vfs_close(vn_stderr);
		kfree(process->pp_fd[0]->file);
		kfree(process->pp_fd[1]->file);
		return ENOMEM;
	}

	strcpy(process->pp_fd[2]->file->p_name, stderr);
	process->pp_fd[2]->file->p_vn = vn_stderr;
	process->pp_fd[2]->file->mode = O_RDWR;
	process->pp_fd[2]->file->flags = O_RDWR;
	process->pp_fd[2]->file->offset = 0;
	process->pp_fd[2]->file->reference_count = 1;

	return 0;
}