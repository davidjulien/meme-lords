/*
 * Driver code for airballoon problem
 */
#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>

#define NROPES 16
static int ropes_left = NROPES;
struct lock *counter_lock;
struct cv *counter_cv;

bool is_finished = false;
struct lock *is_finished_lock;
struct cv *is_finished_cv;

typedef struct rope {
	int rope_stake;
	bool is_cut;
	struct lock * rope_lock;
} rope;

struct rope *ropes[NROPES];

static
int
find_rope_from_stake(int stake)
{
	for (int i = 0; i < NROPES; i++) {
		if (ropes[i]->is_cut)
			continue;
		if (ropes[i]->rope_stake == stake)
			return i;
	}

	return -1;
}

/*
 * Describe your design and any invariants or locking protocols
 * that must be maintained. Explain the exit conditions. How
 * do all threads know when they are done?
 *
 * An array of rope structures is used. Each rope has a stake, a lock, and a flag
 * indicating whether or not it has been cut. While the number of ropes_left > 0,
 * the actors on the ground continuously generate a random number for a stake,
 * and search throught the array of ropes to find their random stake. They then
 * lock the first rope they find that is not cut, and manipulate it according to
 * their required behaviour.
 *
 * The global counter ropes_left also has a lock and a condition variable. When
 * decrementing the ropes_left variable, a lock is acquired on it. The balloon channel
 * waits until the ropes_left is 0 using a condition variable. At that point it
 * frees the memory of the counter lock, the counter cv, and the array of ropes.
 *
 * There is also a global variable flag that tells the main thread that the program is
 * done. The main thread uses a condition variable to wait until the flag is set to
 * true at the end of the ballon thread, then exits the program once signalled.
 */
static
void
dandelion(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Dandelion thread starting\n");

	while (ropes_left > 0) {
		int rope = random() % NROPES;

		if (ropes[rope]->is_cut)
			continue;

		lock_acquire(ropes[rope]->rope_lock);
		//Make sure no one cut the rope while you were acquiring the lock
		if (ropes[rope]->is_cut) {
			lock_release(ropes[rope]->rope_lock);
			continue;
		}
		ropes[rope]->is_cut = true;
		lock_acquire(counter_lock);
		ropes_left--;
		lock_release(counter_lock);
		kprintf("Dandelion severed rope %d\n", rope);
		lock_release(ropes[rope]->rope_lock);
		thread_yield();
	}
	kprintf("Dandelion thread done\n");
	cv_broadcast(counter_cv, counter_lock);
}

static
void
marigold(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Marigold thread starting\n");

	while (ropes_left > 0) {
		// select a random peg k
		int stake = random() % NROPES;
		int rope = find_rope_from_stake(stake);

		if (rope == -1 || ropes[rope]->is_cut)
			continue;

		lock_acquire(ropes[rope]->rope_lock);
		//Make sure no one cut the rope while you were acquiring the lock
		if (ropes[rope]->is_cut) {
			lock_release(ropes[rope]->rope_lock);
			continue;
		}
		ropes[rope]->is_cut = true;
		lock_acquire(counter_lock);
		ropes_left--;
		lock_release(counter_lock);
		kprintf("Marigold severed rope %d from stake %d\n", rope, stake);
		lock_release(ropes[rope]->rope_lock);
		thread_yield();
	}
	kprintf("Marigold thread done\n");
}

static
void
flowerkiller(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Lord FlowerKiller thread starting\n");

	while (ropes_left > 0) {
		// select a random peg k
		int start_stake = random() % NROPES;
		int rope = find_rope_from_stake(start_stake);

		if (rope == -1 || ropes[rope]->is_cut)
			continue;

		int end_stake = random() % NROPES;

		lock_acquire(ropes[rope]->rope_lock);
		if (ropes[rope]->is_cut) {
			lock_release(ropes[rope]->rope_lock);
			continue;
		}
		ropes[rope]->rope_stake = end_stake;
		kprintf("Lord FlowerKiller switched rope %d from stake %d to stake %d\n", rope, start_stake, end_stake);
		lock_release(ropes[rope]->rope_lock);
		thread_yield();
	}
	kprintf("FlowerKiller thread done\n");
}

static
void
balloon(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Balloon thread starting\n");

	lock_acquire(counter_lock);
	while (ropes_left > 0) {
		cv_wait(counter_cv, counter_lock);
	}
	kprintf("Balloon freed and Prince Dandelion escapes!\n");
	lock_release(counter_lock);

	lock_destroy(counter_lock);
	cv_destroy(counter_cv);
	counter_lock = NULL;
	counter_cv = NULL;

	for (int i = 0; i < NROPES; i++) {
		lock_destroy(ropes[i]->rope_lock);
		ropes[i]->rope_lock = NULL;
		kfree((void *)ropes[i]);
		ropes[i] = NULL;
	}
	lock_acquire(is_finished_lock);
	is_finished = true;
	lock_release(is_finished_lock);
	kprintf("Balloon thread done\n");
	cv_broadcast(is_finished_cv, is_finished_lock);
}

// Change this function as necessary
int
airballoon(int nargs, char **args)
{

	int err = 0;

	(void)nargs;
	(void)args;
	(void)ropes_left;

	ropes_left = NROPES;
	is_finished = false;

	is_finished_lock = lock_create("is finished lock");
	if (is_finished_lock == NULL) {
		lock_destroy(is_finished_lock);
		is_finished_lock = NULL;
		kprintf("ERROR: Unable to create finished flag lock!");
		goto done;
	}

	is_finished_cv = cv_create("is finished cv");
	if (is_finished_cv == NULL) {
		lock_destroy(is_finished_lock);
		cv_destroy(is_finished_cv);
		is_finished_lock = NULL;
		is_finished_cv = NULL;
		kprintf("ERROR: Unable to create finished flag condition variable!");
		goto done;
	}

	counter_lock = lock_create("counter lock");
	if (counter_lock == NULL) {
		lock_destroy(counter_lock);
		lock_destroy(is_finished_lock);
		cv_destroy(is_finished_cv);
		counter_lock = NULL;
		is_finished_lock = NULL;
		is_finished_cv = NULL;
		kprintf("ERROR: Unable to create counter lock!");
		goto done;
	}

	counter_cv = cv_create("counter cv");
	if (counter_cv == NULL) {
		lock_destroy(counter_lock);
		cv_destroy(counter_cv);
		lock_destroy(is_finished_lock);
		cv_destroy(is_finished_cv);
		counter_lock = NULL;
		counter_cv = NULL;
		is_finished_lock = NULL;
		is_finished_cv = NULL;
		kprintf("ERROR: Unable to create counter condition variable!");
		goto done;
	}

	// // Initialize rope connections
	for(int i = 0; i < NROPES; i++) {
		struct lock *rope_lock = lock_create("lock");

		if (rope_lock == NULL) {
			lock_destroy(rope_lock);
			// Free all memory in array
			for (int j = 0; j < i; i++) {
				lock_destroy(ropes[i]->rope_lock);
				ropes[i]->rope_lock = NULL;
				kfree((void *)ropes[i]);
				ropes[i] = NULL;
			}
			lock_destroy(counter_lock);
			cv_destroy(counter_cv);
			lock_destroy(is_finished_lock);
			cv_destroy(is_finished_cv);
			counter_lock = NULL;
			counter_cv = NULL;
			is_finished_lock = NULL;
			is_finished_cv = NULL;
			kprintf("Cannot create p_lock_rope for %d\n", i);
			return 0;
		}

		struct rope *r = kmalloc(sizeof(struct rope));
		memcpy(&(r->rope_stake), &i, sizeof(int));
		r->is_cut = false;
		r->rope_lock = rope_lock;
		ropes[i] = r;
	}

	err = thread_fork("Marigold Thread",
			  NULL, marigold, NULL, 0);
	if(err)
		goto panic;
	
	err = thread_fork("Dandelion Thread",
			  NULL, dandelion, NULL, 0);
	if(err)
		goto panic;
	
	err = thread_fork("Lord FlowerKiller Thread",
			  NULL, flowerkiller, NULL, 0);
	if(err)
		goto panic;

	err = thread_fork("Air Balloon",
			  NULL, balloon, NULL, 0);
	if(err)
		goto panic;

	goto done;
panic:
	panic("airballoon: thread_fork failed: %s)\n",
	      strerror(err));
	
done:
	lock_acquire(is_finished_lock);
	while (is_finished == false) {
		cv_wait(is_finished_cv, is_finished_lock);
	}
	lock_release(is_finished_lock);

	lock_destroy(is_finished_lock);
	cv_destroy(is_finished_cv);
	is_finished_lock = NULL;
	is_finished_cv = NULL;
	kprintf("Main thread done\n");
	return 0;
}
