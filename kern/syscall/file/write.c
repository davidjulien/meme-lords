#include <kern/unistd.h>
#include <types.h>
#include <syscall.h>
#include <kern/errno.h>
#include <uio.h>
#include <limits.h>
#include <proc.h>
#include <current.h>
#include <kern/fcntl.h>
#include <vnode.h>
#include <synch.h>


int
sys_write(int fd, const void *buf, size_t nbytes, ssize_t *retval)
{
	int error;

	if (!retval)
	{
		return EFAULT;
	}

	if (is_fd_out_of_bounds(fd))
	{
		return EBADF;
	}

	lock_acquire(curproc->pp_fd[fd]->file_lock);

	if (!curproc->pp_fd[fd]->file ||
		(curproc->pp_fd[fd]->file->flags & O_ACCMODE) == O_RDONLY)
	{
		lock_release(curproc->pp_fd[fd]->file_lock);
		return EBADF;
	}

	if (curproc->pp_fd[fd]->file->offset < 0 ||
		nbytes > END_OF_FILE - curproc->pp_fd[fd]->file->offset)
	{
		lock_release(curproc->pp_fd[fd]->file_lock);
		return ENOSPC;
	}

	struct iovec io_vec;
	io_vec.iov_ubase = (userptr_t)buf;
	io_vec.iov_len = nbytes;

	struct uio u_io;
	uio_init(&u_io,
		&io_vec,
		nbytes,
		curproc->pp_fd[fd]->file->offset,
		UIO_USERSPACE,
		UIO_WRITE,
		proc_getas());

	if ((error = VOP_WRITE(curproc->pp_fd[fd]->file->p_vn, &u_io)))
	{
		lock_release(curproc->pp_fd[fd]->file_lock);
		return error;
	}

	curproc->pp_fd[fd]->file->offset = u_io.uio_offset;
	*retval = nbytes - u_io.uio_resid;

	lock_release(curproc->pp_fd[fd]->file_lock);

	return 0;
}