#include <kern/unistd.h>
#include <types.h>
#include <kern/fcntl.h>
#include <syscall.h>
#include <kern/errno.h>
#include <limits.h>
#include <lib.h>
#include <file.h>
#include <current.h>
#include <proc.h>
#include <copyinout.h>
#include <vfs.h>
#include <vnode.h>
#include <synch.h>


int
sys_open(const char *filename, int flags, int *retval)
{
	if (!retval ||
		!filename)
	{
		return EFAULT;
	}

	int file_permission = flags & O_ACCMODE;
	if (file_permission != O_RDONLY	&&
		file_permission != O_WRONLY &&
		file_permission != O_RDWR)
	{
		return EINVAL;
	}

	int index;
	for (index = 0; index < OPEN_MAX; index++)
	{
		lock_acquire(curproc->pp_fd[index]->file_lock);

		if (!curproc->pp_fd[index]->file)
		{
			break;
		}

		lock_release(curproc->pp_fd[index]->file_lock);
	}

	if (index == OPEN_MAX)
	{
		return EMFILE;
	}

	int error;
	size_t file_name_length;
	char *file_name = (char *)kmalloc(NAME_MAX * sizeof(char));
	if ((error = copyinstr((const_userptr_t)filename, file_name, NAME_MAX, &file_name_length)))
	{
		kfree(file_name);
		lock_release(curproc->pp_fd[index]->file_lock);
		return error;
	}

	struct vnode *p_vn;
	if ((error = vfs_open(file_name, flags, 0, &p_vn)))
	{
		kfree(file_name);
		lock_release(curproc->pp_fd[index]->file_lock);
		return error;
	}

	curproc->pp_fd[index]->file = (struct file *)kmalloc(sizeof(struct file));
	strcpy(curproc->pp_fd[index]->file->p_name, file_name);
	curproc->pp_fd[index]->file->p_vn = p_vn;
	curproc->pp_fd[index]->file->flags = flags;
	curproc->pp_fd[index]->file->mode = O_RDWR;
	curproc->pp_fd[index]->file->offset = 0;
	curproc->pp_fd[index]->file->reference_count = 1;

	kfree(file_name);
	*retval = index;

	lock_release(curproc->pp_fd[index]->file_lock);

	return 0;
}

int
sys_open_mode(const char *filename, int flags, mode_t mode)
{
	(void)filename;
	(void)flags;
	(void)mode;
	return 0;
}