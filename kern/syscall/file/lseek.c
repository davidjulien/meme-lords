#include <kern/unistd.h>
#include <types.h>
#include <syscall.h>
#include <current.h>
#include <kern/seek.h>
#include <current.h>
#include <kern/errno.h>
#include <limits.h>
#include <proc.h>
#include <vnode.h>
#include <stat.h>
#include <synch.h>


off_t get_file_size(struct vnode *);
int is_seek_cur_out_of_bounds(off_t, off_t);
int is_seek_end_out_of_bounds(off_t, off_t, off_t);
int set_offset(int, off_t, int);

int
sys_lseek(int fd, off_t pos, int whence, off_t *retval)
{
	if (!retval)
	{
		return EFAULT;
	}

	*retval = -1;

	if (is_fd_out_of_bounds(fd))
	{
		return EBADF;
	}

	lock_acquire(curproc->pp_fd[fd]->file_lock);

	if (!curproc->pp_fd[fd]->file)
	{
		lock_release(curproc->pp_fd[fd]->file_lock);
		return EBADF;
	}

	if (!VOP_ISSEEKABLE(curproc->pp_fd[fd]->file->p_vn))
	{
		lock_release(curproc->pp_fd[fd]->file_lock);
		return ESPIPE;
	}

	int error;
	if ((error = set_offset(fd, pos, whence)))
	{
		lock_release(curproc->pp_fd[fd]->file_lock);
		return error;
	}

	*retval = curproc->pp_fd[fd]->file->offset;

	lock_release(curproc->pp_fd[fd]->file_lock);

	return 0;
}

int
set_offset(int fd, off_t pos, int whence)
{
	switch (whence)
	{
		case SEEK_SET:
			if (pos < 0)
			{
				return EINVAL;
			}

			curproc->pp_fd[fd]->file->offset = pos;
			break;

		case SEEK_CUR:
			if (is_seek_cur_out_of_bounds(curproc->pp_fd[fd]->file->offset, pos))
			{
				return EINVAL;
			}

			curproc->pp_fd[fd]->file->offset += pos;
			break;

		case SEEK_END:;
			off_t file_size = get_file_size(curproc->pp_fd[fd]->file->p_vn);

			if (is_seek_end_out_of_bounds(curproc->pp_fd[fd]->file->offset, pos, file_size))
			{
				return EINVAL;
			}

			curproc->pp_fd[fd]->file->offset = file_size + pos;
			break;

		default:
			return EINVAL;
	}

	return 0;
}

int
is_seek_cur_out_of_bounds(off_t offset, off_t pos)
{
	return
		offset + pos < 0 ||
		pos > END_OF_FILE - offset;
}

int
is_seek_end_out_of_bounds(off_t offset, off_t pos, off_t file_size)
{
	return
		file_size + pos < 0 ||
		pos > END_OF_FILE - offset;
}

off_t
get_file_size(struct vnode *p_vn)
{
	struct stat file_info;
	VOP_STAT(p_vn, &file_info);

	return file_info.st_size;
}