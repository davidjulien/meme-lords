#include <kern/unistd.h>
#include <types.h>
#include <syscall.h>
#include <kern/errno.h>
#include <vfs.h>
#include <uio.h>
#include <current.h>
#include <proc.h>


int
sys___getcwd(char *buf, size_t buflen, int *retval)
{
	int error;

	if (!retval ||
		!buf)
	{
		return EFAULT;
	}
	
	*retval = -1;

	struct iovec io_vec;
	io_vec.iov_ubase = (userptr_t)buf;
	io_vec.iov_len = buflen;

	struct uio u_io;
	uio_init(&u_io, &io_vec, buflen, 0, UIO_USERSPACE, UIO_READ, curproc->p_addrspace);

	if ((error = vfs_getcwd(&u_io)))
	{
		return error;
	}

	*retval = buflen - u_io.uio_resid;
	
	return 0;
}