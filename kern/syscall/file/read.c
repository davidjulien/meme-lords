#include <kern/unistd.h>
#include <types.h>
#include <syscall.h>
#include <kern/errno.h>
#include <limits.h>
#include <current.h>
#include <kern/fcntl.h>
#include <proc.h>
#include <uio.h>
#include <vnode.h>
#include <synch.h>


ssize_t
sys_read(int fd, void *buf, size_t buflen, int *retval)
{
	int error;

	if (!retval)
	{
		return EFAULT;
	}

	if (is_fd_out_of_bounds(fd))
	{
		return EBADF;
	}

	lock_acquire(curproc->pp_fd[fd]->file_lock);

	if (!curproc->pp_fd[fd]->file ||
		(curproc->pp_fd[fd]->file->flags & O_ACCMODE) == O_WRONLY)
	{
		lock_release(curproc->pp_fd[fd]->file_lock);
		return EBADF;
	}

	if (curproc->pp_fd[fd]->file->offset < 0)
	{
		lock_release(curproc->pp_fd[fd]->file_lock);
		return ENOSPC;
	}

	struct iovec io_vec;
	io_vec.iov_ubase = (userptr_t)buf;
	io_vec.iov_len = buflen;

	struct uio u_io;
	uio_init(&u_io,
		&io_vec,
		buflen,
		curproc->pp_fd[fd]->file->offset,
		UIO_USERSPACE,
		UIO_READ,
		proc_getas());

	if ((error = VOP_READ(curproc->pp_fd[fd]->file->p_vn, &u_io)))
	{
		lock_release(curproc->pp_fd[fd]->file_lock);
		return error;
	}

	curproc->pp_fd[fd]->file->offset = u_io.uio_offset;
	*retval = buflen - u_io.uio_resid;

	lock_release(curproc->pp_fd[fd]->file_lock);

	return 0;
}