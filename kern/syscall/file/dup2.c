#include <kern/unistd.h>
#include <types.h>
#include <syscall.h>
#include <kern/errno.h>
#include <current.h>
#include <limits.h>
#include <proc.h>
#include <synch.h>
#include <vnode.h>


struct file_descriptor* get_empty_file_table_entry(void);
void duplicate_fd(int, int);

int
sys_dup2(int oldfd, int newfd, int *retval)
{
	if (!retval)
	{
		return EFAULT;
	}
	
	*retval = -1;

	if (is_fd_out_of_bounds(oldfd) ||
		is_fd_out_of_bounds(newfd))
	{
		return EBADF;
	}

	lock_acquire(curproc->pp_fd[oldfd]->file_lock);

	if (!curproc->pp_fd[oldfd]->file)
	{
		lock_release(curproc->pp_fd[oldfd]->file_lock);
		return EBADF;
	}

	if (newfd != oldfd)
	{
		lock_acquire(curproc->pp_fd[newfd]->file_lock);

		if (curproc->pp_fd[newfd]->file)
		{
			lock_release(curproc->pp_fd[newfd]->file_lock);
			lock_release(curproc->pp_fd[oldfd]->file_lock);
			return sys_close(newfd);
		}	

		duplicate_fd(oldfd, newfd);
		lock_release(curproc->pp_fd[newfd]->file_lock);
	}

	*retval = newfd;
	
	lock_release(curproc->pp_fd[oldfd]->file_lock);

	return 0;
}

struct file_descriptor*
get_empty_file_table_entry(void)
{
	int i;
	for (i = 0; i < OPEN_MAX; i++)
	{
		lock_acquire(curproc->pp_fd[i]->file_lock);

		if (!curproc->pp_fd[i]->file)
		{
			return curproc->pp_fd[i];
		}

		lock_release(curproc->pp_fd[i]->file_lock);
	}

	return NULL;
}

void
duplicate_fd(int oldfd, int newfd)
{
	curproc->pp_fd[newfd]->file = curproc->pp_fd[oldfd]->file;
	curproc->pp_fd[newfd]->file->reference_count++;
}