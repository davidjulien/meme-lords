#include <kern/unistd.h>
#include <types.h>
#include <syscall.h>
#include <kern/errno.h>
#include <vfs.h>
#include <limits.h>
#include <copyinout.h>


int
sys_chdir(const char *pathname, int *retval)
{
	int error;

	if (!retval ||
		!pathname)
	{
		return EFAULT;
	}
	
	*retval = -1;

	size_t path_name_length;
	char *path_name = (char *)kmalloc(NAME_MAX * sizeof(char));

	if ((error = copyinstr((const_userptr_t)pathname, path_name, NAME_MAX, &path_name_length)) ||
		(error = vfs_chdir(path_name)))
	{
		return error;
	}

	*retval = 0;

	return 0;
}