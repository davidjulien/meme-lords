#include <kern/unistd.h>
#include <types.h>
#include <syscall.h>
#include <limits.h>
#include <proc.h>
#include <current.h>
#include <kern/errno.h>
#include <vnode.h>
#include <vfs.h>
#include <synch.h>


int
sys_close(int fd)
{
	if (is_fd_out_of_bounds(fd))
	{
		return EBADF;
	}

	lock_acquire(curproc->pp_fd[fd]->file_lock);
	struct file *file = curproc->pp_fd[fd]->file;

	if (is_fd_out_of_bounds(fd) ||
		!file)
	{
		lock_release(curproc->pp_fd[fd]->file_lock);
		return EBADF;
	}

	if (file->reference_count == 1)
	{
		vfs_close(file->p_vn);
		kfree(file);
	}
	else
	{
		file->reference_count--;
	}
	
	curproc->pp_fd[fd]->file = NULL;

	lock_release(curproc->pp_fd[fd]->file_lock);

	return 0;
}