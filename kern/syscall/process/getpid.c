#include <kern/unistd.h>
#include <types.h>
#include <kern/fcntl.h>
#include <syscall.h>
#include <kern/errno.h>
#include <limits.h>
#include <lib.h>
#include <current.h>
#include <proc.h>
#include <addrspace.h>
#include <machine/trapframe.h>

int
sys_getpid(pid_t *p_retval)
{
	if (!p_retval)
	{
		return EFAULT;
	}

	*p_retval = curproc->pid;

	return 0;
}