#include <kern/unistd.h>
#include <types.h>
#include <kern/fcntl.h>
#include <syscall.h>
#include <kern/errno.h>
#include <limits.h>
#include <lib.h>
#include <current.h>
#include <proc.h>
#include <addrspace.h>
#include <machine/trapframe.h>
#include <synch.h>
#include <vnode.h>

void decrement_all_ft_reference_counts(void);

void
decrement_all_ft_reference_counts(void)
{
	for (int i  = 0; i < OPEN_MAX; i++)
	{
		lock_acquire(curproc->pp_fd[i]->file_lock);

		if (curproc->pp_fd[i]->file)
		{
			curproc->pp_fd[i]->file->reference_count--;
		}

		lock_release(curproc->pp_fd[i]->file_lock);
	}
}

void
enter_forked_process(void *data1, unsigned long data2)
{
	struct trapframe *p_tf = (struct trapframe*)data1;
	struct addrspace *p_as = (struct addrspace*)data2;

	struct trapframe child_tf = *p_tf;
	child_tf.tf_a3 = 0;
	child_tf.tf_v0 = 0;
	child_tf.tf_epc += 4;

	as_copy(p_as, &curproc->p_addrspace);
	as_activate();
	mips_usermode(&child_tf);
}

int
sys_fork(struct trapframe *p_tf, pid_t *p_retval)
{
	if (!p_tf ||
		!p_retval)
	{
		return EFAULT;
	}

	int error;
	*p_retval = -1;

	struct addrspace *p_child_as;
	if ((error = as_copy(curproc->p_addrspace, &p_child_as)))
	{
		return error;
	}

	// Initialize and copy to child trap frame
	struct trapframe *p_child_tf = (struct trapframe *)kmalloc(sizeof(struct trapframe));
	if (!p_child_tf)
	{
		as_destroy(p_child_as);
		return ENOMEM;
	}

	*p_child_tf = *p_tf;

	// Initialize child process and set address space
	struct proc *child_proc = proc_create_runprogram(curproc->p_name);

	if (!child_proc)
	{	
		kfree(p_child_tf);
		as_destroy(p_child_as);
		return ENOMEM;
	}

	// Copy current process file table to child process file table
	for (int i = 0; i < OPEN_MAX; i++)
	{
		lock_acquire(curproc->pp_fd[i]->file_lock);

		child_proc->pp_fd[i] = curproc->pp_fd[i];

		if (child_proc->pp_fd[i]->file)
		{
			child_proc->pp_fd[i]->file->reference_count++;
		}

		lock_release(curproc->pp_fd[i]->file_lock);
	}

	// Add the child process to the process ID table
	if (proc_pid_table_add(child_proc))
	{
		kfree(p_child_tf);
		as_destroy(p_child_as);
		decrement_all_ft_reference_counts();
		proc_destroy(child_proc);
		return ENOMEM;
	}

	// Add the child to the current process's child process table
	if (proc_add_child(child_proc))
	{
		kfree(p_child_tf);
		as_destroy(p_child_as);
		decrement_all_ft_reference_counts();
		proc_destroy(child_proc);
		return ENOMEM;
	}

	// Run child process
	if ((error = thread_fork(curthread->t_name, child_proc, enter_forked_process, p_child_tf, (unsigned long)p_child_as)))
	{
		kfree(p_child_tf);
		as_destroy(p_child_as);
		decrement_all_ft_reference_counts();
		pid_table[child_proc->pid]->proc = NULL;
		proc_destroy(child_proc);

		return error;
	}

	*p_retval = child_proc->pid;

	return 0;
}