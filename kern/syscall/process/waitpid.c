#include <types.h>
#include <syscall.h>
#include <kern/errno.h>
#include <limits.h>
#include <proc.h>
#include <current.h>
#include <synch.h>
#include <copyinout.h>


void wait_for_child_exit(struct proc*);
struct proc* find_child_process(pid_t);

int
sys_waitpid(pid_t pid, int *p_status, int options, pid_t *p_retval)
{
	if (!p_retval)
	{
		return EFAULT;
	}

	*p_retval = -1;

	if (options)
	{
		return EINVAL;
	}

	if (pid < PID_MIN ||
		pid > PID_MAX)
	{
		return ESRCH;
	}

	struct proc *child = find_child_process(pid);

	if (!child)
	{
		return ECHILD;
	}

	wait_for_child_exit(child);

	int error;

	if (p_status &&
		(error = copyout((void *)(&(child->exit_code)), (userptr_t)p_status, sizeof(int))))
	{
		return error;
	}

	*p_retval = pid;
	return 0;
}

void
wait_for_child_exit(struct proc* child)
{
	lock_acquire(child->p_wait_lock);

	while (!child->exit_status)
	{
		cv_wait(child->p_wait_cv, child->p_wait_lock);
	}

	lock_release(child->p_wait_lock);
}

struct proc*
find_child_process(pid_t pid)
{
	for (int i = PID_MIN; i < PID_MAX; i++)
	{
		if (curproc->children_procs[i]->child &&
			curproc->children_procs[i]->child->pid == pid)
		{
			return curproc->children_procs[i]->child;
		}
	}

	return NULL;
}