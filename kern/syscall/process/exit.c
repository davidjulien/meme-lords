#include <types.h>
#include <syscall.h>
#include <kern/errno.h>
#include <limits.h>
#include <proc.h>
#include <current.h>
#include <synch.h>
#include <kern/wait.h>
#include <addrspace.h>


void wait_for_parent_process_to_exit(struct proc *);
void free_child_processes_exit_locks(struct proc *);

void
sys__exit(int exitcode)
{
	struct proc *proc = curproc;

	proc->exit_code = _MKWAIT_EXIT(exitcode);
	proc->exit_status = true;

	free_child_processes_exit_locks(proc);

	// Free the child processes's wait locks
	cv_broadcast(proc->p_wait_cv, proc->p_wait_lock);

	as_deactivate();
	struct addrspace *as = proc_setas(NULL);
	as_destroy(as);

	proc_remthread(curthread);

	wait_for_parent_process_to_exit(proc);

	pid_table[proc->pid]->proc = NULL;
	proc_destroy(proc);
	
	thread_exit();
}

void
wait_for_parent_process_to_exit(struct proc *proc)
{
	lock_acquire(proc->p_exit_lock);
	lock_release(proc->p_exit_lock);
}

void
free_child_processes_exit_locks(struct proc *proc)
{
	for (int i = PID_MIN; i < PID_MAX; i++)
	{
		if (proc->children_procs[i]->child)
		{
			lock_release(proc->children_procs[i]->child->p_exit_lock);
			proc->children_procs[i]->child = NULL;
		}
	}
}