#include <types.h>
#include <syscall.h>
#include <kern/unistd.h>
#include <kern/errno.h>
#include <limits.h>
#include <lib.h>
#include <copyinout.h>
#include <vfs.h>
#include <addrspace.h>
#include <kern/fcntl.h>
#include <vm.h>
#include <test.h>
#include <proc.h>

void kfree_array(char **array, int size);

void
kfree_array(char **array, int size)
{
	for (int i = 0; i < size; i++)
	{
		kfree(array[i]);
	}

	kfree(array);
}

int
sys_execv(const char *p_program, char **pp_args, int *p_retval)
{
	if (!p_program ||
		!pp_args ||
		!p_retval)
	{
		return EFAULT;
	}

	*p_retval = -1;
	int total_size_of_args = 0;
	int argc = 0;

	// Add up the lengths of every argument and check that it is less than ARG_MAX before continuing
	for (argc = 0; pp_args[argc]; argc++)
	{
		total_size_of_args += strlen(pp_args[argc]) + 1;
	}

	if (total_size_of_args > ARG_MAX)
	{
		return E2BIG;
	}

	int program_length = strlen(p_program) + 1;	
	char *kprogram = (char *)kmalloc(program_length * sizeof(char));

	// Copy user-level program string to kernel-level program string
	int error;
	if ((error = copyinstr((const_userptr_t)p_program, kprogram, program_length, NULL)))
	{
		kfree(kprogram);
		return error;
	}

	char **kargs = (char **)kmalloc(argc * sizeof(char *));
	int total_args_size = 0;

	// Copy user-level arguments to kernel-level arguments
	for (int i = 0; i < argc; i++)
	{
		int arg_length = strlen(pp_args[i]) + 1;
		kargs[i] = (char *)kmalloc(arg_length * sizeof(char));

		if ((error = copyinstr((const_userptr_t)pp_args[i], kargs[i], arg_length, NULL)))
		{
			kfree_array(kargs, argc);
			kfree(kprogram);
			return error;
		}

		// Calculate aligned length and decrement stackptr
		int aligned_arg_length = arg_length +
			(arg_length % 4 == 0 ?
				0 :
				(4 - arg_length % 4));

		total_args_size += (sizeof(char) * aligned_arg_length) + sizeof(char *);
	}

	total_args_size += sizeof(char *);

	struct addrspace *as = NULL;
	struct vnode *v = NULL;
	vaddr_t entrypoint = 0;
	vaddr_t stackptr = 0;

	/* Open the file. */
	if ((error = vfs_open(kprogram, O_RDONLY, 0, &v))) {
		kfree_array(kargs, argc);
		kfree(kprogram);
		return error;
	}

	/* Create a new address space. */
	as = as_create();
	if (!as) {
		kfree_array(kargs, argc);
		kfree(kprogram);
		vfs_close(v);
		return ENOMEM;
	}

	/* Switch to it and activate it. */
	proc_setas(as);
	as_activate();

	/* Load the executable. */
	if ((error = load_elf(v, &entrypoint))) {
		kfree_array(kargs, argc);
		kfree(kprogram);
		/* p_addrspace will go away when curproc is destroyed */
		vfs_close(v);
		return error;
	}

	/* Done with the file now. */
	vfs_close(v);

	/* Define the user stack in the address space */
	if ((error = as_define_stack(as, &stackptr))) {
		kfree_array(kargs, argc);
		kfree(kprogram);
		/* p_addrspace will go away when curproc is destroyed */
		return error;
	}

	// Allocate enough memory on the stackptr for the arguments and the pointers to the arguments
	stackptr -= total_args_size;

	char **pp_temp_args = (char **)kmalloc(argc * sizeof(char *));
	int string_offset = sizeof(char *) * (argc + 1); // Add 1 for the NULL pointer at the end

	// Copy kernel-level arguments to stack pointer
	for (int i = 0; i < argc; i++)
	{
		// Arguments in the stackptr are 4-byte aligned
		int arg_length = strlen(kargs[i]) + 1;
		int aligned_arg_length = arg_length + (arg_length % 4 == 0 ? 0 : (4 - arg_length % 4));
		char *arg = (char *)kmalloc(aligned_arg_length * sizeof(char));
		memset(arg, '\0', sizeof(char) * aligned_arg_length);
		memcpy(arg, kargs[i], arg_length);

		size_t temp_string_offset = 0;
		userptr_t string_destination = (userptr_t)(stackptr + string_offset);

		if ((error = copyoutstr((const char *)arg, string_destination, aligned_arg_length, &temp_string_offset)))
		{
			kfree_array(pp_temp_args, argc);
			kfree_array(kargs, argc);
			kfree(kprogram);
			return error;
		}

		pp_temp_args[i] = (char *)string_destination;
		string_offset += temp_string_offset;
	}

	pp_temp_args[argc] = NULL;

	if ((error = copyout(pp_temp_args, (userptr_t)stackptr, (argc + 1) * sizeof(char *))))
	{
		kfree_array(pp_temp_args, argc);
		kfree_array(kargs, argc);
		kfree(kprogram);
		return error;
	}

	kfree_array(kargs, argc);
	kfree(kprogram);

	/* Warp to user mode. */
	enter_new_process(argc,
		(userptr_t)stackptr,
		NULL,
		stackptr,
		entrypoint);

	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
	return EINVAL;

	return 0;
}